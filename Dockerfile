FROM centos

RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

RUN yum update -y
RUN yum install httpd -y
RUN echo "Welcome to *RLAB$ httpd V3.4" >> /var/www/html/index.html
EXPOSE 80
CMD ["httpd", "-D", "FOREGROUND"]
